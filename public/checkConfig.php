<?php

function checkConfig($configPath)
{
    try {
        if (!file_exists($configPath)) {
            throw new Exception('Configuration file not exists.');
        }

        require $configPath;

        if (!isset($APP_CONFIG['open_weather_api_host']) || is_null($APP_CONFIG['open_weather_api_host'])) {
            throw new Exception('Configuration property is missing - open_weather_api_host');
        }
        if (!isset($APP_CONFIG['open_weather_api_token'])) {
            throw new Exception('Configuration property is missing - open_weather_api_token');
        }
        if (!isset($APP_CONFIG['open_weather_api_units'])) {
            throw new Exception('Configuration property is missing - open_weather_api_units');
        }
    } catch(Exception $exception) {
        echo $exception->getMessage();
        die();
    }


}