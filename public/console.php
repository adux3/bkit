<?php

require 'checkConfig.php';
checkConfig('./config.php');

$action = $argv[1];
$argument = $argv[2];

require 'autoloader.php';
require './config.php';

$_ENV['APP_OWA_HOST'] = $APP_CONFIG['open_weather_api_host'];
$_ENV['APP_OWA_TOKEN'] = $APP_CONFIG['open_weather_api_token'];
$_ENV['APP_OWA_UNITS'] = $APP_CONFIG['open_weather_api_units'];

use Core\Controller;

try {
    if ($action === null || $argument === null) {
        throw new Exception ('Bad command!');
    }

    $dto = Controller::processEvent($action, $argument);
    echo $dto->getConsoleOutput();
} catch(Exception $exception) {
    echo $exception->getMessage();
}
