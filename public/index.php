<?php

require 'checkConfig.php';
checkConfig('../config.php');

require 'autoloader.php';
require '../config.php';

$_ENV['APP_OWA_HOST'] = $APP_CONFIG['open_weather_api_host'];
$_ENV['APP_OWA_TOKEN'] = $APP_CONFIG['open_weather_api_token'];
$_ENV['APP_OWA_UNITS'] = $APP_CONFIG['open_weather_api_units'];

use Core\Controller;

try {
    $dto = Controller::processEvent('city', 'London');
    echo $dto->getHtmlOutput();
} catch(Exception $exception) {
    echo $exception->getMessage();
}
