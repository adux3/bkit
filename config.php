<?php
return $APP_CONFIG = [
    'open_weather_api_host' => 'http://api.openweathermap.org/data/2.5/',
    'open_weather_api_token' => 'TOKEN',
    'open_weather_api_units' => 'metric'
];
