FROM php:7.4-fpm-buster

RUN apt-get update \
    && apt-get install -y \
      wget \
      nano
RUN wget -O phpunit https://phar.phpunit.de/phpunit-9.phar
RUN chmod +x phpunit

WORKDIR /var/www/html

EXPOSE 80