# Weather APP

## Description
Proof of concept of weather app. 

## Technologies
- Docker 20.10  
- Php 7.4

## Installation
```bash
(sudo) docker-compose build
```

After that you need to set your environment variables in config.php

## Running
```bash
(sudo) docker-compose up
```

## Go to app console
When app is running: 

```bash
(sudo) docker exec -it prod-weather bash
```

## Commands

**All commands must be running from app console!**

### Get Weather By City

```
sh ./run.sh city {:city}
```

#### Example

```
sh ./run.sh city London
```

## Tests
**Unit tests must be running from app console!**

```
sh ./test.sh
```

## Preview Web Server
When app is running, you can go to http://192.168.101.201/ in your browser.   
index.php returns weather for London if app works correctly.
