<?php

namespace Shared;
use Shared\Command;

interface CommandHandler
{
    public function execute(Command $command);
}