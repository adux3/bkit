<?php

namespace Shared;

interface DTO
{
    public function getConsoleOutput(): string;
    public function getHtmlOutput(): string;
}