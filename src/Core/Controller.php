<?php
declare(strict_types=1);

namespace Core;

use Weather\Application\Command\{GetWeatherByCityCommand};
use Weather\Application\Handler\{GetWeatherByCityHandler};
use Weather\Application\Builder\{WeatherRepositoryBuilder};

class Controller
{
    public static function processEvent(string $event, string $argument)
    {
        $command = self::getCommandByEvent($event, $argument);
        $handler = self::getHandlerByCommand(self::getCommandName($command));
        return $handler->execute($command);
    }

    private static function getCommandByEvent(string $event, string $argument) {
        switch ($event) {
            case 'city':
                return GetWeatherByCityCommand::createByCity($argument);
        }
    }

    private static function getHandlerByCommand(string $command)
    {
        switch ($command) {
            case 'GetWeatherByCityCommand':
                $owaClient = WeatherRepositoryBuilder::createFromConfigData();
                return new GetWeatherByCityHandler($owaClient);
        }
    }

    private static function getCommandName(object $command)
    {
        $namespaceClass = explode("\\", get_class($command));
        return end($namespaceClass);
    }
}