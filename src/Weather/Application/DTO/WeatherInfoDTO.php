<?php
declare(strict_types=1);

namespace Weather\Application\DTO;

use Shared\DTO;

class WeatherInfoDTO implements DTO
{
    /* @todo unit needs to be setting by config->open_weather_api_units */
    const TEMPERATURE_INFO = 'degrees celcius';

    /* @var string */
    private $description;

    /* @var int */
    private $temperature;

    public function __construct(string $description, int $temperature)
    {
        $this->description = $description;
        $this->temperature = $temperature;
    }

    public function getConsoleOutput(): string
    {
        return $this->outputWeather();
    }

    public function getHtmlOutput(): string
    {
        return '<h1>'.$this->outputWeather().'</h1>';
    }

    private function outputWeather(): string
    {
        return $this->description.', '.$this->temperature.' '.self::TEMPERATURE_INFO;
    }

    public static function createFromApiResponse(Object $response): self
    {
        return new WeatherInfoDTO($response->weather[0]->description, (int) $response->main->temp);
    }
}