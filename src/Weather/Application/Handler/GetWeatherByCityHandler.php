<?php
declare(strict_types=1);

namespace Weather\Application\Handler;

use Shared\CommandHandler;
use Weather\Infrastructure\Repository\WeatherRepository;
use Weather\Application\Command\GetWeatherByCityCommand;
use Shared\Command;

use Weather\Application\DTO\WeatherInfoDTO;

class GetWeatherByCityHandler implements CommandHandler
{
    /* @var WeatherRepository */
    private $weatherRepository;

    public function __construct(WeatherRepository $weatherRepository)
    {
        $this->weatherRepository = $weatherRepository;
    }

    /* @var GetWeatherByCityCommand $command */
    public function execute(Command $command)
    {
        $stdResponse = $this->weatherRepository->getWeatherByCity($command->getCity());
        return WeatherInfoDTO::createFromApiResponse($stdResponse);
    }
}