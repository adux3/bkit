<?php
declare(strict_types=1);

namespace Weather\Application\Builder;

use Weather\Infrastructure\Client\OpenWeatherMapClient;
use Weather\Infrastructure\Repository\WeatherRepository;

class WeatherRepositoryBuilder
{
    public static function createFromConfigData(): WeatherRepository
    {
        $openWeatherMapClient = new OpenWeatherMapClient($_ENV['APP_OWA_HOST'], $_ENV['APP_OWA_TOKEN'], $_ENV['APP_OWA_UNITS']);
        return new WeatherRepository($openWeatherMapClient);
    }
}