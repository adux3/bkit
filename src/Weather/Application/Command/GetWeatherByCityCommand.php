<?php
declare(strict_types=1);

namespace Weather\Application\Command;

use Shared\Command;

class GetWeatherByCityCommand implements Command
{
    /* @var string */
    private $city;

    public static function createByCity(string $city): self
    {
        return new GetWeatherByCityCommand($city);
    }

    public function __construct(string $city)
    {
        $this->city = $city;
    }

    public function getCity(): string
    {
        return $this->city;
    }
}