<?php
declare(strict_types=1);

namespace Weather\Infrastructure\Client;

use Exception;

class OpenWeatherMapClient
{
    /* @var string */
    private $host;

    /* @var string */
    private $token;

    /* @var string */
    private $units;

    /* @var string */
    private $path = '';

    /* @var array */
    private $pathArguments = [];

    /* @var string */
    private $method = 'GET';

    /* @var array */
    private $headers = [];

    /* @var array */
    private $body = [];

    public function __construct(string $host, string $token, string $units)
    {
        $this->host = $host;
        $this->token = $token;
        $this->units = $units;
    }

    public function setPath(string $path): self
    {
        $this->path = $path;
        return $this;
    }

    public function setMethod(string $method): self
    {
        $this->method = $method;
        return $this;
    }

    public function setBody(array $body): self
    {
        $this->body = $body;
        return $this;
    }

    public function setHeaders(array $headers): self
    {
        $this->headers = $headers;
        return $this;
    }

    public function setPathArguments(array $pathArguments): self
    {
        $this->pathArguments = $pathArguments;
        return $this;
    }

    public function execute(): Object
    {
        $url = $this->host.$this->path.$this->getTransformedPathArguments();
        $options = [
            'http' => [
                'header'  => $this->getTransformedHeaders(),
                'method'  => $this->method,
                'content' => http_build_query($this->body)
            ]
        ];
        $context  = stream_context_create($options);

        if (false === $result = @file_get_contents($url, false, $context)) {
            /* Limited by file_get_contents */
            throw new Exception('Response data is empty or unable to connection with external service.');
        }

        return json_decode($result);
    }

    private function getTransformedHeaders(): string
    {
        return http_build_query($this->headers,'',"\r\n");
    }

    private function getTransformedPathArguments(): string
    {
        $this->injectAuthorizationToken();
        $this->injectUnits();
        return count($this->pathArguments) > 0 ? '?'.http_build_query($this->pathArguments,'',"&") : '';
    }

    private function injectAuthorizationToken()
    {
        $this->pathArguments['appid'] = $this->token;
    }

    private function injectUnits()
    {
        $this->pathArguments['units'] = $this->units;
    }
}