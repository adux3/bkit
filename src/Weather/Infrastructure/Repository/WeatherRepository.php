<?php
declare(strict_types=1);

namespace Weather\Infrastructure\Repository;

use Weather\Infrastructure\Client\OpenWeatherMapClient;

class WeatherRepository
{
    /* @var OpenWeatherMapClient */
    private $openWeatherMapClient;

    public function __construct(OpenWeatherMapClient $openWeatherMapClient)
    {
        $this->openWeatherMapClient = $openWeatherMapClient;
    }

    public function getWeatherByCity(string $city): Object
    {
        $arguments = [
            'q' => $city
        ];

        return $this->openWeatherMapClient->setPath('weather')->setPathArguments($arguments)->execute();
    }
}