<?php
declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Weather\Application\Command\GetWeatherByCityCommand;
use Weather\Application\Handler\GetWeatherByCityHandler;

final class GetWeatherByCityTest extends TestCase
{
    const CITY = 'London';

    public function testCommandCreateCorrectlyForString(): void
    {
        $command = new GetWeatherByCityCommand(self::CITY);
        $this->assertEquals($command->getCity(), self::CITY);
    }

    public function testCommandNotCreateAndThrowExceptionForNull(): void
    {
        $this->expectException(TypeError::class);
        new GetWeatherByCityCommand(null);
    }

    public function testHandlerExecutedCorrectlyIfCommandIsGood(): void
    {
        $handler = $this->createMock(GetWeatherByCityHandler::class);
        $handler->method('execute')->willReturn('goodAnswer');

        $command = new GetWeatherByCityCommand(self::CITY);

        $this->assertEquals('goodAnswer', $handler->execute($command));
    }
}
