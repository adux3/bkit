<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Weather\Application\Builder\WeatherRepositoryBuilder;
use Weather\Infrastructure\Repository\WeatherRepository;

final class WeatherRepositoryBuilderTest extends TestCase
{
    public function testWeatherRepositoryIsBuildingCorrectlyForDefaultConfig()
    {
        $_ENV['APP_OWA_HOST'] = 'aa';
        $_ENV['APP_OWA_TOKEN'] = 'bb';
        $_ENV['APP_OWA_UNITS'] = 'cc';
        $repo = WeatherRepositoryBuilder::createFromConfigData();
        $this->assertInstanceOf(WeatherRepository::class, $repo);
    }
}
