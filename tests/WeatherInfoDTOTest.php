<?php

declare(strict_types=1);

use PHPUnit\Framework\TestCase;
use Weather\Application\Builder\WeatherRepositoryBuilder;
use Weather\Application\DTO\WeatherInfoDTO;
use Weather\Infrastructure\Repository\WeatherRepository;

final class WeatherInfoDTOTest extends TestCase
{
    const TEMP = 30;
    const DESCRIPTION = 'SUN';

    public function testDtoCreatedCorrectlyFromApiResponse()
    {
        $main = new stdClass();
        $main->temp = self::TEMP;

        $weather = new stdClass();
        $weather->description = self::DESCRIPTION;

        $response = new stdClass();
        $response->main = $main;
        $response->weather[] = $weather;

        $dto = WeatherInfoDTO::createFromApiResponse($response);

        $this->assertEquals($dto->getConsoleOutput(), self::DESCRIPTION.', '.self::TEMP.' '.WeatherInfoDTO::TEMPERATURE_INFO);
    }
}
